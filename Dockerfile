FROM gcr.io/bitnami-containers/minideb-extras:jessie-r14

MAINTAINER Bitnami <containers@bitnami.com>

COPY ./go-k8s /app/go-k8s

USER bitnami

WORKDIR /app

EXPOSE 3000

ENTRYPOINT ["/app/go-k8s"]

