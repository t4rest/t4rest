#!/usr/bin/env bash

gometalinter --checkstyle --deadline 1h --skip=vendor ./... > report.xml