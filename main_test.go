package main

import "testing"

func TestService_Sum(t *testing.T) {
	r, err := Sum(1, 2)
	if err != nil {
		t.Errorf("expected err nil, got %s", err)
	}

	if r != 3 {
		t.Errorf("expected = %d, got %d", 3, r)
	}
}

func TestService_Sumq(t *testing.T) {
	r, err := Sumq(1, 2)
	if err != nil {
		t.Errorf("expected err nil, got %s", err)
	}

	if r != 3 {
		t.Errorf("expected = %d, got %d", 3, r)
	}
}

func TestService_Sumw(t *testing.T) {
	r, err := Sumw(1, 2)
	if err != nil {
		t.Errorf("expected err nil, got %s", err)
	}

	if r != 3 {
		t.Errorf("expected = %d, got %d", 3, r)
	}
}

func TestService_Sume(t *testing.T) {
	r, err := Sume(1, 2)
	if err != nil {
		t.Errorf("expected err nil, got %s", err)
	}

	if r != 3 {
		t.Errorf("expected = %d, got %d", 3, r)
	}
}

func TestService_Sumr(t *testing.T) {
	r, err := Sumr(1, 2)
	if err != nil {
		t.Errorf("expected err nil, got %s", err)
	}

	if r != 3 {
		t.Errorf("expected = %d, got %d", 3, r)
	}
}

func TestService_Sumt(t *testing.T) {
	r, err := Sumt(1, 2)
	if err != nil {
		t.Errorf("expected err nil, got %s", err)
	}

	if r != 3 {
		t.Errorf("expected = %d, got %d", 3, r)
	}
}

func TestService_Sumy(t *testing.T) {
	r, err := Sumy(1, 2)
	if err != nil {
		t.Errorf("expected err nil, got %s", err)
	}

	if r != 3 {
		t.Errorf("expected = %d, got %d", 3, r)
	}
}
