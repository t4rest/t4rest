# cyren/fetcher

gometalinter --checkstyle --enable misspell --enable gofmt --deadline 1h --skip=vendor ./... > report.xml
gocov test ./... | gocov-xml > coverage.xml
go test -v ./... | go-junit-report > test.xml


sonar
```
sonar-scanner
```